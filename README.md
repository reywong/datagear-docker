2021-01-18 更新-2

稳定版升级到2.1.1，停止运行的容器（如果正在运行的话），然后在启动就行，操作命令如下：

```bash
# 需要在你clone的项目目录内
docker-compose down
# 运行前编译镜像，防止更新版本号后，不编译的情况
docker-compose up --build
```



2021-01-18 更新

添加运行演示（PS：需要docker及docker-compose，请提前安装好环境）

**开发版**每次docker-compose up都会拉取代码，保持代码最新，编译并启动
![img](images/datagear-dev.gif)



**稳定版**以DataGear 2.1.0为例，以后更新版本操作相同
![img](images/datagear-2.1.0.gif)



2021-01-07 更新

增加源码构建运行，相关文件在dev目录， 进入dev目录，执行：docker-compose up --build 即可！ 外部映射端口为：50501，访问使用：http://ip:50501


2021-01-05

使用Docker部署DataGear 2.1.0，克隆仓库进入目录执行：docker-compose up --build 即可！访问使用：http://ip:50401

方便参数配置采用 docker-compose 运行！

不想使用 docker-compose 的话，直接build Dockerfile 启动就行

DataGear仓库：https://gitee.com/datagear/datagear

DataGear文档：http://www.datagear.tech/documentation/